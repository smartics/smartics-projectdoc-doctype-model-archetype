# Doctype Model Archetype

## Overview

This [archetype](https://maven.apache.org/guides/introduction/introduction-to-archetypes.html)
creates projects based on [Maven](https://maven.apache.org/) to create
[doctypes](https://www.smartics.eu/confluence/x/pYmj) for the
[projectdoc Toolbox](https://www.smartics.eu/confluence/x/GQFk) for
[Confluence](https://www.atlassian.com/software/confluence).

The generated project is supposed to contain
[model descriptors](https://www.smartics.eu/confluence/x/SgHqAw) for doctypes.
A doctype is based on a [blueprint](https://confluence.atlassian.com/doc/blueprints-323982376.html)
and typically provides one [template](https://confluence.atlassian.com/doc/page-templates-296093785.html)
to create [projectdoc documents](https://www.smartics.eu/confluence/x/rgDG).

A project created with this archetype will be configured to generate a
[doctype add-on](https://www.smartics.eu/confluence/x/nQHJAw) project (which
produces an [OSGi Bundle Repository (OBR)](https://developer.atlassian.com/docs/faq/advanced-plugin-development-faq/bundling-extra-dependencies-in-an-obr)
to be installed to your Confluence server). You typically will use this
archetype though a mojo provides by the [Doctype Maven Plugin](https://www.smartics.eu/confluence/x/zgDqAw).

So the sequence of actions is this:

1. `mvn doctype:create-model` - creates the model project with a demo model
2. `cd [the new model project]` - change to the new project
3. `mvn package` - creates the add-on project (using the [smartics-projectdoc-doctype-addon-archetype](https://bitbucket.org/smartics/smartics-projectdoc-doctype-addon-archetype)) and generates the Confluence blueprints based on the demo model
4. `cd target/[the new add-on project]` - change to the created add-on project
5. `atlas-debug` - to start Confluence with the demo blueprints

Sounds easy? Well, it isn't. You need to have to meet some
[prerequisites](https://www.smartics.eu/confluence/x/jgL1Aw) to get this
started. This includes installing the Atlassian SDK, having the required
artifacts pushed to your repository and configuring your development
environment. But once this is done, well - yeah - it is that easy!


## Documentation

Models are specified with [XML](https://www.w3.org/XML/) files - a little
verbose maybe, but we provide [schema files](https://www.w3.org/XML/Schema) to
support users to type them with their favorite XML editor.

Please have a look at the [model documentation](https://www.smartics.eu/confluence/x/SgHqAw).
There you can check if our approach is the right one for you. It will also show
you the limitations of what can currently be expressed with the models and what
is still about to be supported.

Check out more information on the [projectdoc Toolbox](https://www.smartics.eu/confluence/x/1YEp),
its [Online Manual](https://www.smartics.eu/confluence/x/EAFk), and the
[Doctypes Maven Plugin](https://www.smartics.eu/confluence/x/zgDqAw)!

On our [homepage](https://www.smartics.eu/) you'll find contact information to
get in touch.


## Fork me!
Feel free to fork this project to adjust the archetype to your project requirements.

This archetype project is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
